@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">{{$data['name']}}---{{$data['age']}}</div>

				<div class="panel-body">
					@foreach($data['article'] as $k=>$v)
                        @if($k>1)
                        {{$v}}
                        @endif
                        @endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
